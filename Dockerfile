  FROM phusion/passenger-ruby27

  RUN curl https://deb.nodesource.com/setup_12.x | bash && \
  curl https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
  echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
  apt-get update -qq && apt-get install -y build-essential libpq-dev software-properties-common nodejs yarn default-jdk-headless && \
  update-alternatives --config java && \
  export JAVA_HOME=$(dirname $(dirname $(readlink $(readlink $(which javac))))) && \
  apt-get clean && \
  npm install -g npm && \
  npm cache clean --force

  RUN mkdir /app
  WORKDIR /app
  ADD Gemfile /app/Gemfile
  ADD Gemfile.lock /app/Gemfile.lock
  RUN bundle install
  ADD . /app
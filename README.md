# Phil Rails Solr

## Overview 
This is a testing playground for getting Ruby on Rails / Solr running in Docker.

It is based off of this tutorial (among other random stuff I found on the web):
[Mount a Solr search engine with Rails using Docker Compose](http://blog.magmalabs.io/2019/05/30/mount-a-solr-search-engine-with-rails-using-docker-compose.html)

## Requirements

1. [Docker Desktop](https://www.docker.com/products/docker-desktop)

## Installation
To get started with this project, use the following commands:

```
# Clone the repo to your git
> git clone https://gitlab.com/phil-plencner-hl/phil-rails-solr.git

# Enter the folder that was created by the clone
> cd phil-rails-solr

# Generate the rails skeleton
> docker-compose run web rails new . --force --no-deps --database=postgresql

# Build the docker images
> docker-compose up --build

# Break out of the running app and shut down the containers
> cntrl-C
> docker-compose down

# Copy database.yml into the config folder
> cp database.yml config/database.yml

# Create the database
> docker-compose run web rake db:create

# Build the docker images
> docker-compose up --build

```

The rails app will be here:
http://localhost:3000/

The Solr server will be here:
http://localhost:8983
